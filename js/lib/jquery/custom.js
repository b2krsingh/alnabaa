var p="";
//header user account
jQuery(document).ready(function(){
jQuery(".user_account").hover(function(){
    jQuery(this).children(".user_option").stop(true, false, true).fadeToggle(); 
});
//End header user account
// Search bar    
jQuery(".search_bar_mobile input").click(function(){
    jQuery(".overlay").css({"display":"block","transition": "all 0.4s ease"});
});
    jQuery(".mobile_search_bar, .navbar-form a").click(function(){
    jQuery(".overlay").css({"display":"none","transition": "all 0.4s ease"});
});
// Search bar
//menu on mobile
    jQuery(window).scroll(function(){ 
        if(jQuery(".header_wrapper").offset().top>70)
            {
                jQuery(".my_nav").addClass("anim_menu");
            }
        else{
            jQuery(".my_nav").removeClass("anim_menu");
        }
         if (jQuery(window).width() <= 767) {
            if (jQuery('.search_bar').offset().top > 150) {               
                jQuery('.search_bar').addClass('fixSearch');
            }
            else {
               jQuery('.search_bar').removeClass('fixSearch');
            }

        }
        else if (jQuery(window).width() >= 768 && jQuery(window).width() <= 991) {
            if (jQuery('.search_bar').offset().top > 150) {               
                jQuery('.search_bar').addClass('fixSearch');
            }
            else {
               jQuery('.search_bar').removeClass('fixSearch');
            }

        }
        else{
             if(jQuery('.search_bar').hasClass('fixSearch')==true){jQuery('.search_bar').removeClass('fixSearch');}
        }
    });   
    //menu on mobile Ends
p=jQuery('.Pslider').owlCarousel(owlOptions ={
    items:4,
    loop:true,
    slideBy: 6,
    nav : true,
    navText: ["<span class='carousel-nav-left'><i class='fa fa-chevron-left'></i></span>","<span class='carousel-nav-right'><i class='fa fa-chevron-right'></i></span>"],
    dots: false,
    paginationSpeed: 300,
    rewindSpeed: 400,
    responsive:{
        0:{
            items:2,
            nav:true
        },
        600:{
            items:3,
            nav:true
        },
        1000:{
            items:6,
            nav:true
        }
    }
});
    
    if (jQuery(window).width() <= 991) {
        p.trigger('destroy.owl.carousel');
        p.find('.owl-stage-outer').children(':eq(0)').unwrap();
        jQuery('.owl-carousel').addClass('items3').css({"display":"block"});
        
    } else {
        jQuery('.Pslider').owlCarousel(owlOptions);jQuery('.owl-carousel').removeClass('items3');
    } 
    
jQuery(window).resize(function () {
    if (jQuery(window).width() <= 991) {
        p.trigger('destroy.owl.carousel');
        p.find('.owl-stage-outer').children(':eq(0)').unwrap();
        jQuery('.owl-carousel').css({"display":"block"});
        
    } else {
        jQuery('.Pslider').owlCarousel(owlOptions);jQuery('.owl-carousel').removeClass('items3');
    }
}); 


jQuery('.Lslider').owlCarousel({
    items:8,
    loop:true,
    nav : true,
    slideBy: 4,
    navText: ["<span class='carousel-nav-left'><i class='fa fa-chevron-left'></i></span>","<span class='carousel-nav-right'><i class='fa fa-chevron-right'></i></span>"],
    dots: false,
    paginationSpeed: 300,
    rewindSpeed: 400,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        600:{
            items:3,
            nav:true
        },
        1000:{
            items:7,
            nav:true
        }
    }
});
    
jQuery('#slider').owlCarousel({
     items: 1,
     loop: true,
     margin:0,
     nav: true,
     smartSpeed: 900,
    dots: true,
    pagination:true
    // navText: ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"] 
});
//    $('.my_nav ul li').hover(function () {
//       $(this).find('.sub_nav').fadeToggle(700);
//});
//    $(".navbar-toggle").click(function(){
//        $(".collapse").css({"width":"280px","transition": "all .4s ease 0s"});
//        $(".overlay2").fadeIn();
//    });
//    $(".overlay2").click(function(){
//        $(".collapse").css({"width":"0px"});
//        $(".overlay2").css({"display":"none","transition": "all .4s ease 0s"});
//    });
    
    if (jQuery(window).width() <= 991) {
            jQuery('.my_nav ul li').click(function () {
       jQuery(this).find('.sub_nav').slideToggle(400);
});
        }
        else if(jQuery(window).width() >= 992)
        {
            jQuery('.my_nav ul li').hover(function () {
//       $(this).find('.sub_nav').fadeToggle(700);
                jQuery(this).children(".sub_nav").stop(true, false, true).fadeToggle(250);
});
        }
    
    
    jQuery(".navbar-toggle").click(function(){        
       jQuery(".overlay2").fadeIn();
        jQuery(".collapse").css({"left":"0px"});
    });
    jQuery(".overlay2").click(function(){        
       jQuery(".overlay2").fadeOut();
        jQuery(".collapse").css({"left":"-280px"});
    });
    
    //Profile Address Remove
    
    jQuery(".address_detail ul li i").click(function(){
        jQuery(this).parent("li").remove();
    })
    //End Profile Address Remove
    
});





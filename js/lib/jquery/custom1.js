(function($){
 jQuery(document).ready(function () {

            jQuery('.slider-for').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: true,
                arrows: false,
                fade: false,
                asNavFor: '.slider-nav'
            });
            jQuery('.slider-nav').slick({
                slidesToShow: 4,
                slidesToScroll: 1,
                asNavFor: '.slider-for',
                dots: false,
                centerMode: true,
                focusOnSelect: true
            });
        });
  jQuery('.selectpicker').selectpicker({
            style: 'btn-info',
            size: 4
        }); 
        var marginSlider = document.getElementById('slider-margin');
        noUiSlider.create(marginSlider, {
            start: [2500, 5500],
            margin: 30,
            range: {
                'min': 500,
                'max': 10000
            }
        });
        // var marginMin = document.getElementById('slider-margin-value-min'),
        // marginMax = document.getElementById('slider-margin-value-max');
        var min="5000";
        var min="7000";
        marginSlider.noUiSlider.on('update', function (min, max) {
            console.log("values : "+min+" handle : "+min);           
        });
        jQuery('.sidebar_wrapper .grpTitle').click(function(){  
            jQuery(this).siblings('ul').slideToggle(500);
            jQuery(this).toggleClass('active');
        });
        jQuery('.sdr-title').click(function(){
          if($(window).width()<768){
            // $(this).siblings('.allfilter').slideToggle(800);
            // $(this).toggleClass('active');  
          }
        });
 /* equal height */
 equalheight = function(container){
    var currentTallest = 0,
        currentRowStart = 0,
        rowDivs = new Array(),
        $el,
        topPosition = 0;
    jQuery(container).each(function() {
        $el = $(this);
        $($el).height('auto')
        topPostion = $el.position().top;

        if (currentRowStart != topPostion) {
            for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
                rowDivs[currentDiv].height(currentTallest);
            }
            rowDivs.length = 0; // empty the array
            currentRowStart = topPostion;
            currentTallest = $el.height();
            rowDivs.push($el);
        } else {
            rowDivs.push($el);
            currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
        }
        for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
            rowDivs[currentDiv].height(currentTallest);
        }
    });

    jQuery('.navbar-nav li a').click(function(){
        //alert('h');
        jQuery('.navbar-collapse').removeClass('in');
    });


}

jQuery(window).ready(function() {    
    equalheight('.eql_height');
    //  if($(window).width()<768){
    //       $('.sdr-title').addClass('arrowDown');
    //       $('.allfilter, .sidebar_wrapper > ul').css({'display':'none'});
    //       $('.grpTitle, .sdr-title').removeClass('active');       
    //     }
    jQuery('#mfilter').on('click',function(e){
        e.stopPropagation();
         jQuery('.product_listing_left').addClass('active');
         jQuery('body').append('<div class="overlay222"> </div>');

         
    });
jQuery('.sdr-title h2').click(function(){
    if(jQuery(window).width()<768){

       jQuery('.product_listing_left').removeClass('active');
       jQuery('.overlay222').remove();
    }
});
jQuery('#msort').on('click',function(){
    jQuery('ul.sortList').slideToggle(500);
});

    
});
 jQuery(window).resize(function () {
     equalheight('.eql_height');
     //filter
    //     if($(window).width()<768){
    //       $('.sdr-title').addClass('arrowDown');
    //       $('.allfilter, .sidebar_wrapper > ul').css({'display':'none'});
    //       $('.grpTitle, .sdr-title').removeClass('active');          
    //     }else{
    //      $('.sdr-title').removeClass('arrowDown');
    //      $('.allfilter, .sidebar_wrapper > ul').css({'display':'block'});
    //      $('.grpTitle, .sdr-title').removeClass('active'); 
    //   }
});
jQuery(window).scroll(function(){    
    console.log();
   if(jQuery('.mobSort').offset().top > 225){jQuery('.mobSort').addClass('fixedOnTop'); }
  
});

})(jQuery);

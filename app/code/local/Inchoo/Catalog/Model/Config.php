<?php
class Inchoo_Catalog_Model_Config extends Mage_Catalog_Model_Config 

            { 

                public function getAttributeUsedForSortByArray() 

                { 

                    return array_merge( 
			array('most_view' => Mage::helper('catalog')->__('Popularity')),
                        array('qty_ordered' => Mage::helper('catalog')->__('Best Seller')),
			parent::getAttributeUsedForSortByArray() 

                    ); 

                } 

            }  
?>

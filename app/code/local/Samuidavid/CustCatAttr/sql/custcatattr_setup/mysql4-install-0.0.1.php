<?php
$this->startSetup();
$this->addAttribute(
    Mage_Catalog_Model_Category::ENTITY,
    'category_icon',
    array(
        'group' => 'General Information',
        'input' => 'textarea',
        'type' => 'text',
        'label' => 'Menu icon',
        'backend' => '',
        'visible'                  => true,
        'required'                 => false,
        'wysiwyg_enabled'          => true,
        'visible_on_front'         => true,
        'is_html_allowed_on_front' => true,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    )
);
$this->endSetup();
?>
